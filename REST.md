# RESTful Web Service
- Web Application vs Web Service
    - Web Application vs Web Service
        - "Web Application" usually refers to a completed software for use by human beings
            - Backend information is stored and processed
            - Frontend allows huymans through nice graphics like button, forms and table to access the backend information
            - Both of those things working together is a web application.
        - "Web Service"
            - Just the backend information storage and processing
            - Web Services are designed to be **language agnostic** for the people using them.
    - **REST**
        - **RE**presentation **S**tate **T**ransfer
        - A architectural style for a web service
            - Probably the most common type of web service on the web.
        - **Resources**
            - A resource is a collection of objects that you are keeping track of.
                - a landlord might have a REST service that keeps track of *apartments*
                - a hospital has a REST service that keeps track of *appointments*
                - a restaurant micht have a REST service that keeps track of *orders*

            - Resources *can* be nested
                - One resource might contain other resources
                - You can keep track of *clients* who have their own *accounts*
- **Representation**
    - Rather than returning the resource itself you return a representation of it in a formation that the client wants.
    - 99% of the time its JSON
        - Technically in any format
        - XML (2nd most popular)
        - could be in PDF
        - PNG. some custom format
    - It should be in a data format that is universal and not specific to a programming language
- Constraints of REST
    - 6 constraints that must be met in order to be truly RESTful
- **Uniform Interface**
    - The standard URI routes and paths used to interact with your Resources
        - URL vs URI
            - URL(Uniform Resource Location) espn.com/teams/pirates
                - Has the domain name attached
            - URI (Uniform Resource Identifier)/teams/pirates
    - Example routes for a pet store keeping trak of pets
        - GET /pets
            - Returns all pets
        - GET /pets/30
            - Return the pet with an ID of 30
        - POST /pets
            - Create a new pet
        - PUT /pets/19
            - Update/replace the pet with the ID of 19
        - DELETE /pets/65
            - Delete the pet eith the ID of 65
        - PATCH /pets/5
            - Update/edit the pet with ID of 65
        - PATCH /pets/6/sold
            - Edit the pet with ID 6 tobe marked as sold
    - **CRUD** operations
        - Create
            - POST
        - Read
            - GET
        - Update
            - PATCH
        - Delete
            - DELETE
- npm dependencies for this project
npm i express
npm i @types/express
npm i jest
npm i @types/jest
npm i ts-jest
npm i typescript

- npm i will install all the dependencies in the package.json file... so you can copy and paste that file into the project.. then run npm i
