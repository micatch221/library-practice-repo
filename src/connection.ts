import {Client} from 'pg';

export const client = new Client({
    user:'postgres',
    password:process.env.dbPassword,
    database:'librarydb',
    port:5432,
    host:'104.196.142.138'

})
client.connect()