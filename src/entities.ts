// An entity is a class that stores information that will ultimately be persisted somewhere
// Usually very minimal logic
// They SHOULD ALWAYS have one field in that that is a unique identifier

export class Book{
    constructor(
        public bookId:number,
        public title:string,
        public author:string,
        public isAvailable:boolean,
        public quality:number,
        public returnDate:number //Typically dates are store in unix epoch time wihch is second from midnight January 1970
    ){}
}
