import express from 'express';
import { Book } from './entities';
import BookService from './services/book-service';
import { BookServiceImpl } from './services/book-service-impl';

const app = express();
app.use(express.json()); // Middleware


// Our applicaiton routes should use services we create to do the heavy lifting
// try to minimize the amount of logic in your routes that is NOT related directly to HTTP requests and responses
const bookService:BookService = new BookServiceImpl();

app.get("/books", async (req, res) => {
    const books:Book[] = await bookService.retrieveAllBooks();
    res.send(books);
});

app.get("/books/:id", async (req, res)=>{
    const bookId = Number(req.params.id);
    const book:Book = await bookService.retrieveBookById(bookId);
    res.send(book);
});

app.post("/books", async (req,res) =>{
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book);
});

app.patch("/books/:id/checkout", async (req, res)=>{
    const bookid = Number(req.params.id);
    const book = await bookService.checkoutBookById(bookid);
    res.send(book)
});

app.listen(3000,()=>{console.log("Application Started")});