import { BookDAO } from "../src/daos/book-dao";
import { Book } from "../src/entities";
import { BookDaoPostgres } from "../src/daos/book-dao-postgres";

const bookDAO:BookDAO = new BookDaoPostgres();

//Any entity object that has NOT been saved somewhere should have the id of 0
// This is a standard software convention
const testBook:Book = new Book(0,'The Hobbit','Tolkien', true, 1, 0);

test("Create a book", async ()=>{
    const result:Book = await bookDAO.createBook(testBook);
    expect(result.bookId).not.toBe(0);// An entity that is saved should have non-zero id 
});

// An integration test requires that two or more functions you wrote pass
test("Get book by Id", async ()=>{
    let book:Book = new Book(0,'Dracula', "Bram Stoker",true,1,0);
    book = await bookDAO.createBook(book);

    let retrievedBook:Book = await bookDAO.getBookById(book.bookId); // avoid hard coded id values in a test
    // difficult to maintain

    expect(retrievedBook.title).toBe(book.title);
});

test("Get all books", async ()=>{
    let book1:Book = new Book(0,'Sapiens','Yuval',true,0,0);
    let book2:Book = new Book(0,'1984',"George Orwell",true,1,1);
    let book3:Book = new Book(0,'Paradox of choice','Barry Schwartz',true,0,0);
    await bookDAO.createBook(book1);
    await bookDAO.createBook(book2);
    await bookDAO.createBook(book3);

    const books:Book[] = await bookDAO.getAllBooks();

    expect(books.length).toBeGreaterThanOrEqual(3);
});

test("update book", async ()=>{
    let book:Book = new Book(0,'We have always lived in the castle','Shirley Jackson',true,1,0);
    book = await bookDAO.createBook(book);

    // to update an object we just edit it and then pass it into a method
    book.quality = 4;
    book = await bookDAO.updateBook(book);

    const updatedBook = await bookDAO.getBookById(book.bookId);
    expect(updatedBook.quality).toBe(4);
});

test("delete book by id", async ()=>{
    let book:Book = new Book(0,'Frankenstien','Mary Shelley',true,1,0);
    book = await bookDAO.createBook(book);

    const result:boolean = await bookDAO.deleteBookById(book.bookId);
    expect(result).toBeTruthy()

});

// Try to avoid methods in your program that return void
// methods that do not return data are VERY difficult to test
// for the most part you want your methods to take in objects